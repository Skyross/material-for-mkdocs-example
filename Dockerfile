FROM python:3.9-slim AS base

ENV POETRY_NO_INTERACTION=1 \
    POETRY_VERSION=1.1.13 \
    POETRY_VIRTUALENVS_CREATE=false \
    PYTHONPATH=.

WORKDIR /opt/app


FROM base AS builder

RUN apt update \
    && apt upgrade -y --no-install-recommends \
    && apt install git -y --no-install-recommends \
    && apt clean \
    && rm -rf /var/lib/apt/lists/*

# Install python required packages
RUN pip install --no-cache-dir pip==22.0.3 "poetry==${POETRY_VERSION}" \
    && poetry --version

# Prepare dependencies
COPY README.md poetry.lock pyproject.toml ./
RUN poetry install -vv --no-dev

CMD ["mkdocs", "--help"]
