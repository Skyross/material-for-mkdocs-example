# 2. Choose documentation static site generator

Date: 2022-06-21

## Status

Accepted

## Context

In order to have common approach and style across all projects within company,
we need to choose documentation static site generator.

## Decision

We decided to use [Material for MkDocs][1] because we used it in the past and
there no significant features that other options have.

[1]: https://squidfunk.github.io/mkdocs-material/

## Consequences

Since we have Markdown as common format, we can easily switch to any other
static site generator with a minimum efforts.

---

## Considered options

Many available options are listed at [GitLab Pages examples][2]. That is
handful!

[2]: https://gitlab.com/pages?sort=stars_desc

### Hugo

Site: [https://gohugo.io/][3.1]

Example: [https://gitlab.com/pages/hugo][3.2]

_The world’s fastest framework for building websites._

Hugo is one of the most
popular open-source static site generators. With its
amazing speed and flexibility, Hugo makes building websites fun again.

!!! warning
    Add more details about why Hugo is best for your company in a pros/cons
    format.

[3.1]: https://gohugo.io/
[3.2]: https://gitlab.com/pages/hugo

### Jekyll

Site: [http://jekyllrb.com/][4.1]

Example: [https://gitlab.com/pages/jekyll][4.2]

_Transform your plain text into static websites and blogs._

!!! warning
    Add more details about why Jekyll is best for your company in a pros/cons
    format.

[4.1]: http://jekyllrb.com/
[4.2]: https://gitlab.com/pages/jekyll

### Plain HTML

Example: [https://gitlab.com/pages/plain-html][5]

_"Old but gold"._

!!! warning
    Add more details about why Plain HTML is best for your company in a
    pros/cons format.

[5]: https://gitlab.com/pages/plain-html

### GitBook

Site: [https://www.gitbook.com/][6.1]

Example: [https://gitlab.com/pages/gitbook][6.2]

_Where software teams break knowledge silos._

GitBook helps you publish beautiful docs for your users and centralize your
teams' knowledge for advanced collaboration.

!!! warning
    Add more details about why GitBook is best for your company in a
    pros/cons format.

[6.1]: https://www.gitbook.com/
[6.2]: https://gitlab.com/pages/gitbook

### Hexo

Site: [https://hexo.io/][7.1]

Example: [https://gitlab.com/pages/hexo][7.2]

_A fast, simple & powerful blog framework._

!!! warning
    Add more details about why Hexo is best for your company in a
    pros/cons format.

[7.1]: https://hexo.io/
[7.2]: https://gitlab.com/pages/hexo

### Sphinx

Site: [https://www.sphinx-doc.org/en/master/][8.1]

Example: [https://gitlab.com/pages/sphinx][8.2]

Sphinx is a tool that makes it easy to create intelligent and beautiful
documentation, written by Georg Brandl and licensed under the BSD license.

It was originally created for the Python documentation, and it has excellent
facilities for the documentation of software projects in a range of languages.

!!! warning
    Add more details about why Sphinx is best for your company in a
    pros/cons format.

[8.1]: https://www.sphinx-doc.org/en/master/
[8.2]: https://gitlab.com/pages/sphinx

### Material for MkDocs

Site: [https://squidfunk.github.io/mkdocs-material/][9.1]

Example: [https://gitlab.com/Skyross/material-for-mkdocs-example][9.2]

_Documentation that simply works._

Write your documentation in Markdown and create a professional static site in
minutes – searchable, customizable, for all devices.

!!! warning
    Add more details about why Material for MkDocs is best for your company in a
    pros/cons format.

[9.1]: https://squidfunk.github.io/mkdocs-material/
[9.2]: https://gitlab.com/Skyross/material-for-mkdocs-example
