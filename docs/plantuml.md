# PlantUML examples

[PlantUML][0] is a component that allows to quickly write:

- Sequence diagram
- Usecase diagram
- Class diagram
- Object diagram
- Activity diagram (here is the legacy syntax)
- Component diagram
- Deployment diagram
- State diagram
- Timing diagram

The following non-UML diagrams are also supported:

- JSON data
- YAML data
- Network diagram (nwdiag)
- Wireframe graphical interface or UI mockups (salt)
- Archimate diagram
- Specification and Description Language (SDL)
- Ditaa diagram
- Gantt diagram
- MindMap diagram
- Work Breakdown Structure diagram (WBS)
- Mathematic with AsciiMath or JLaTeXMath notation
- Entity Relationship diagram (IE/ER)

Furthermore:

- Hyperlinks and tooltips
- Creole: rich text, emoticons, unicode, icons
- OpenIconic icons
- Sprite icons
- AsciiMath mathematical expressions

[0]: https://plantuml.com/en/

## Examples

!!! note
Examples were taken from [REAL WORLD PlantUML][1].

[1]: https://real-world-plantuml.com/

### Sequence diagrams

[https://real-world-plantuml.com/umls/4606798564687872][2]

```plantuml
@startuml

participant "app: Application" as app
participant "cm: ContentManager" as cm
participant "item: DownloadItem" as item

activate app
activate cm

note over app: User enters media info page

note over app: Check if item exists
app->cm: findItem(itemId)
cm->cm: lookup(itemId)

alt item found
    cm-->app: item
else not found
    cm-->app: null
    app->cm: createItem(itemId, contentURL)
    cm->item: new(itemId, contentURL)
    activate item
    cm-->app: item

    app->cm: loadMetadata()
    note over cm
        Download and parse manifest, save in db
    end note
    cm-->app: onTracksAvailable
    cm-->app: onDownloadMetadata
    note over app: * See //track-selection// flow
end group

note over app: app is ready to start downloading
app->item: startDownload()

@enduml
```

??? "Source code"

    ```text
    @startuml

    participant "app: Application" as app
    participant "cm: ContentManager" as cm
    participant "item: DownloadItem" as item

    activate app
    activate cm

    note over app: User enters media info page

    note over app: Check if item exists
    app->cm: findItem(itemId)
    cm->cm: lookup(itemId)

    alt item found
        cm-->app: item
    else not found
        cm-->app: null
        app->cm: createItem(itemId, contentURL)
        cm->item: new(itemId, contentURL)
        activate item
        cm-->app: item

        app->cm: loadMetadata()
        note over cm
            Download and parse manifest, save in db
        end note
        cm-->app: onTracksAvailable
        cm-->app: onDownloadMetadata
        note over app: * See //track-selection// flow
    end group

    note over app: app is ready to start downloading
    app->item: startDownload()

    @enduml
    ```

[2]: https://real-world-plantuml.com/umls/4606798564687872

### Component diagram

[https://real-world-plantuml.com/umls/4619683198140416][3]

```plantuml
@startuml
package "ArduCopter - Simple Version" {
  [EnginesControl] -down-> Engines
  [EnginesControl] - [MainCopterProcess]
  [MainCopterProcess] - [Rangefinder]
  [Rangefinder] -down-> BottomSonicSensor
  [MainCopterProcess] -down- [GPSSignalListener]
}
package "CarDuino Nano" {
  [GPSSignalMaker] -down- [MainCarDuinoProcess]
  [MainCarDuinoProcess] -down- [CommandListener]
  [GPSSignalMaker] -up- [GPSSignalSender]
  [MainCarDuinoProcess] - [5x Rangefinders]
  [5x Rangefinders] -down-> 5xSonicSensors
  [TelemetricsSender] - [MainCarDuinoProcess]
  [TelemetricsSender] -down- MiniUSB
  [CommandListener] -left- MiniUSB
}
package "Intell 2800 - Simple Version" {
  [ComputerCommunications] -up- USB
  [ComputerCommunications] - [MainComputerProcess]
  [KinectProcessing] -down-> KINECT
  [KinectProcessing] - [MainComputerProcess]
  [VideoProcessing] -down-> Camera
  [VideoProcessing] - [MainComputerProcess]
  [ComputerCommunications2] -up- [MainComputerProcess]
  [ComputerCommunications2] -down- WiFi
  [ComputerCommunications2] -down- Bluetooth
}
[GPSSignalListener] -down- [GPSSignalSender]
USB -up- MiniUSB
@enduml
```

??? "Source code"

    ```text
    @startuml
    package "ArduCopter - Simple Version" {
      [EnginesControl] -down-> Engines
      [EnginesControl] - [MainCopterProcess]
      [MainCopterProcess] - [Rangefinder]
      [Rangefinder] -down-> BottomSonicSensor
      [MainCopterProcess] -down- [GPSSignalListener]
    }
    package "CarDuino Nano" {
      [GPSSignalMaker] -down- [MainCarDuinoProcess]
      [MainCarDuinoProcess] -down- [CommandListener]
      [GPSSignalMaker] -up- [GPSSignalSender]
      [MainCarDuinoProcess] - [5x Rangefinders]
      [5x Rangefinders] -down-> 5xSonicSensors
      [TelemetricsSender] - [MainCarDuinoProcess]
      [TelemetricsSender] -down- MiniUSB
      [CommandListener] -left- MiniUSB
    }
    package "Intell 2800 - Simple Version" {
      [ComputerCommunications] -up- USB
      [ComputerCommunications] - [MainComputerProcess]
      [KinectProcessing] -down-> KINECT
      [KinectProcessing] - [MainComputerProcess]
      [VideoProcessing] -down-> Camera
      [VideoProcessing] - [MainComputerProcess]
      [ComputerCommunications2] -up- [MainComputerProcess]
      [ComputerCommunications2] -down- WiFi
      [ComputerCommunications2] -down- Bluetooth
    }
    [GPSSignalListener] -down- [GPSSignalSender]
    USB -up- MiniUSB
    @enduml
    ```

[3]: https://real-world-plantuml.com/umls/4619683198140416

### Activity diagram

[https://real-world-plantuml.com/umls/4886556628221952][4]

```plantuml
@startuml
title Servlet Container

(*) --> "ClickServlet.handleRequest()"
--> "new Page"

if "Page.onSecurityCheck" then
  ->[true] "Page.onInit()"

  if "isForward?" then
   ->[no] "Process controls"

   if "continue processing?" then
     -->[yes] ===RENDERING===
   else
     -->[no] ===REDIRECT_CHECK===
   endif

  else
   -->[yes] ===RENDERING===
  endif

  if "is Post?" then
    -->[yes] "Page.onPost()"
    --> "Page.onRender()" as render
    --> ===REDIRECT_CHECK===
  else
    -->[no] "Page.onGet()"
    --> render
  endif

else
  -->[false] ===REDIRECT_CHECK===
endif

if "Do redirect?" then
 ->[yes] "redirect request"
 --> ==BEFORE_DESTROY===
else
 if "Do Forward?" then
  -left->[yes] "Forward request"
  --> ==BEFORE_DESTROY===
 else
  -right->[no] "Render page template"
  --> ==BEFORE_DESTROY===
 endif
endif

--> "Page.onDestroy()"
-->(*)

@enduml
```

??? "Source code"

    ```text
    @startuml
    title Servlet Container

    (*) --> "ClickServlet.handleRequest()"
    --> "new Page"

    if "Page.onSecurityCheck" then
      ->[true] "Page.onInit()"

      if "isForward?" then
       ->[no] "Process controls"

       if "continue processing?" then
         -->[yes] ===RENDERING===
       else
         -->[no] ===REDIRECT_CHECK===
       endif

      else
       -->[yes] ===RENDERING===
      endif

      if "is Post?" then
        -->[yes] "Page.onPost()"
        --> "Page.onRender()" as render
        --> ===REDIRECT_CHECK===
      else
        -->[no] "Page.onGet()"
        --> render
      endif

    else
      -->[false] ===REDIRECT_CHECK===
    endif

    if "Do redirect?" then
     ->[yes] "redirect request"
     --> ==BEFORE_DESTROY===
    else
     if "Do Forward?" then
      -left->[yes] "Forward request"
      --> ==BEFORE_DESTROY===
     else
      -right->[no] "Render page template"
      --> ==BEFORE_DESTROY===
     endif
    endif

    --> "Page.onDestroy()"
    -->(*)

    @enduml
    ```

[4]: https://real-world-plantuml.com/umls/4886556628221952
